###Requirements:
1. PHP version 7.0+
2. configured email server

###Configuration:
email could be changed in the config file "config.json"

###Description:
All not sent emails could be found in the "email" folder

Application generate form key and validate it after form submition, 
it prevent possibility for cross site data submit. All user input data are validated 
