<?php

class Engine
{
    private $_config;

    private $_fields = [
        'required' => [
            'name',
            'email',
            'contact_method',
            'info'
        ],
        'optional' => [
            'phone',
            'invoice'
        ],
    ];

    public function __construct($opts)
    {
        session_start();
        $this->_initConfig($opts);
    }

    public function run()
    {
        if ($this->_isSubmit()) {
            $this->_handleSubmit();
        } else {
            $this->_renderForm();
        }
    }

    public function getFormKey()
    {
        $hash = md5(uniqid(rand(), true));
        $_SESSION['formkey'] = $hash;
        return $hash;
    }



    private function _sendEmail($data)
    {
        if (empty($this->_config['email'])) {
            throw new Exception('email is invalid');
        }
        $str = print_r($data, true);
        try {
            set_error_handler('warning_handler', E_WARNING);
            $status = mail($this->_config['email'], 'New Request', $str);
            restore_error_handler();
        } catch (Exception $e) {
            $this->_saveEmail($str);
            throw $e;
        }

        if (!$status) {
            $this->_saveEmail($str);
        }
        return $status;
    }

    private function _saveEmail($str)
    {
        $path = __DIR__ . DIRECTORY_SEPARATOR . 'email';
        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }
        file_put_contents($path . DIRECTORY_SEPARATOR . 'email_' . time(), $str);
    }

    private function _handleSubmit()
    {
        try {
            $this->_validate();
            $data = $this->_getData();
            $status = $this->_sendEmail($data);
            $this->_sendResponse($status, '');
        } catch (Exception $e) {
            $this->_sendResponse(false, $e->getMessage());
        }
    }

    private function _validate()
    {
        $post = $this->_getPost();
        if (!empty($post['form_key']) && $post['form_key'] === $_SESSION['formkey']) {
            return true;
        }
        throw new Exception('form data is invalid');
    }

    private function _initConfig($opts)
    {
        if (empty($opts['config']) || !$content = file_get_contents($opts['config'])) {
            throw new Exception('config not set');
        }
        $this->_config = json_decode($content, true);
    }

    private function _isSubmit()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST'
            && !empty($_SERVER['HTTP_X_REQUESTED_WITH'])
            && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest'
        ) {
            return true;
        }
        return false;
    }

    private function _getPost()
    {
        parse_str(file_get_contents('php://input'), $post);
        return $post;
    }

    private function _getData()
    {
        $post = $this->_getPost();
        $data = [];
        foreach ($this->_fields['required'] as $field) {
            if (empty($post[$field])) {
                throw new Exception('invalid data');
            }
            $data[$field] = $this->_parse($post[$field]);
        }
        foreach ($this->_fields['optional'] as $field) {
            $data[$field] = $this->_parse($post[$field]);
        }
        return $data;
    }

    private function _parse($data)
    {
        return strip_tags($data);
    }

    private function _renderForm()
    {
        if (empty($this->_config['template'])) {
            throw new Exception('form template is invalid');
        }
        ob_start();
        require $this->_config['template'];
        $content = ob_get_clean();
        echo $content;
    }

    private function _sendResponse($status, $message)
    {
        echo json_encode([
            'status' => $status,
            'msg' => $message
        ]);
    }
}
