<?php

require_once 'functions.php';
require_once 'Engine.php';

$config = [
    'config' => __DIR__ . DIRECTORY_SEPARATOR . 'config.json'
];
$app = new Engine($config);
$app->run();
